/*

  AutoDA_equipementInfo : script utilisateur pour faciliter la saisie d'une
  demande d'achat depuis un fournisseur du marché Matinfo dans l'outil de
  gestion des commandes de l'Inria.

  Copyright (C) 2020-2021 de

  Centre de recherche Inria Sophia Antipolis Méditerranée,
  Équipe Factas,
  Sophia Antipolis, France.

  Auteur : S. Chevillard
  sylvain.chevillard@ens-lyon.org

  Ce programme est un logiciel libre: vous pouvez le redistribuer
  et/ou le modifier selon les termes de la "GNU General Public
  License", tels que publiés par la "Free Software Foundation"; soit
  la version 3 de cette licence ou (à votre choix) toute version
  ultérieure.

  Ce programme est distribué dans l'espoir qu'il sera utile, mais
  SANS AUCUNE GARANTIE, ni explicite ni implicite; sans même les
  garanties de commercialisation ou d'adaptation dans un but spécifique.

  Se référer à la "GNU General Public License" pour plus de détails.
  Elle est disponible ici : http://www.gnu.org/licenses/gpl-3.0.html

  Vous devriez avoir reçu une copie de la "GNU General Public License"
  en même temps que ce programme; sinon, écrivez a la "Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA".

*/

// ==UserScript==
// @name         AutoDA_equipementInfo
// @namespace    http://gitlab.inria.fr/
// @version      5.0
// @description  Réaliser automatiquement la DA associée à une demande d'équipement informatique
// @author       Sylvain Chevillard
// @include      http*://v2.dell-matinfo.fr/*
// @include      https://sourcing.econocom.com/*
// @include      https://b2b.hpe.com/*
// @include      https://prod-oebs-app.inria.fr:4453/*
// @require      https://code.jquery.com/jquery-3.3.1.min.js
// @require      https://code.jquery.com/ui/1.12.0/jquery-ui.min.js
// @grant GM_setValue
// @grant GM_getValue
// @grant GM_deleteValue
// ==/UserScript==

(function() {
    'use strict';

    /* Initialise les préférences utilisateur */
    if (!GM_getValue('prefs')) {
	GM_setValue('prefs', {"centre":"04"});
    }

    /* Recherche le séparateur décimal utilisé par SafIn */
    function trouveSeparateurDecimal() {
	var scripts = document.querySelectorAll("script");
	var res = null;
	for(const elem of scripts) {
	    if (res = /_decimalSep\s*=\s*['"](.)['"]/.exec(elem.innerHTML)) {
		return res[1];
	    }
	}
	return ","; /* Par défaut */
    }
    var decimalSep = trouveSeparateurDecimal();

    /* Float vers str utilisant les préférences enregistrées */
    function floatVersStr(x) {
	var sep = decimalSep;
	return x.toString().replace('.', sep).replace(',', sep);
    }

    /* Vérifie que elem est non vide, et s'il a un attribut list, vérifie que son contenu est bien conforme */
    function inputEstValide(elem) {
	if (elem.value == "") return false;
	if (elem.list) {
	    for (const op of elem.list.options) {
		if (op.value == elem.value) return true;
	    }
	    return false;
	}
	return true;
    }

    /* Propage la catégorie de l'élément qui vient de perdre le focus à tous
     * les éléments en dessous dans le même groupe d'options d'un devis */
    function propageCategorie(elem) {
	var valeur = elem.value;
	var trParent = elem.parentNode.parentNode;
	var trCourant = trParent;
	while(trCourant = trCourant.nextSibling) {
	    trCourant.querySelector("td:nth-child(2) input").value = valeur;
	}
    }

    function afficheDevis(s) {
	var conteneur = document.createElement('div');
	conteneur.id = "conteneurDevis";
	var str = '';
	str+= '<div id="afficheDevis">\n';
	str+= 'Équipe <input type="text" size="10" id="equipe" value="'+s.equipe+'"><span></span>\n';
	str+= 'N° de devis <input type="text" id="numeroDevis" value="'+s.numero+'"><span></span>\n';
	str+= 'Clavier <input type="text" size="7" id="clavier" value="'+s.clavier+'"><span></span>\n';
	str+= 'Fournisseur <input type="text" size="15" id="fournisseur" value="'+document.getElementById("SupplierOnNonCat").value+'" disabled><span></span>\n';
	str+= '<input type="hidden" size="15" id="SupplierName" value="'+s.SupplierName+'">\n';
	str+= 'Site <input type="text" size="10" id="site" value="'+document.getElementById("SupplierSiteOnNonCat").value+'" disabled><span></span>\n';
	str+= '<input type="hidden" size="10" id="SupplierSiteId" value="'+s.SupplierSiteId+'">\n';
	str+= '<button id="changePrefs">\n';
	str+= ' <img src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAACXBIWXMAAABjAAAAYwEq1VM0AAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAaZJREFUKJF1kz2IE0EUx39vZtyUcsgFxXiJilhoFwvBNRlJlUZURBsLEdTC0vYqC7HSQhvPQrCwCWJlkSDZCWmvPAtF2eA3yolWh+jO2NzJXNh9zfDe/P7vYx4jlFi3270qIitbvojczLLs7jynysTA0djx3h8pgwTAWns+hHDSGPPQe78YQngM7I+4T0qpK977zyJyDVjNsuyJpGm6YIx5DSxWdFFmv7TWh5Ux5laFMABfN89521kUxR0VQjhQcjkBlpxze4qi2Au8LGEaql6vnxaRe1HwT5IkF5xzHwGm0+mXWq12EdjYAkTkPtBXg8Gg8N4//99rCB9Go9G3uMRwOPwBvItCL5xzf1W/368Bl6KsS9ba3bG41+vtAg5GBc602+0dutFoOBE5G7EKON5qtbLZbPaz0+nsA54ChyLmWJIkJwywBqRzj5ECubX2OxUrFJE3Smu9DKxvtuPZvpqq3a9rrZd1nucbzWYzV0q99d5fFpFnwClgIZrxvdb6nFLqdgjht4g8Go/Hq1KW1lr7ALgRiVcmk8n1ea7qY6xtg5R6VQb9A9K6jrb/sjirAAAAAElFTkSuQmCC"></img>\n';
	str+= '</button><br>\n';
	str+= '<table id="afficheDevisTable">\n';
	str+= '<thead><tr><th>Description</th><th style="width: 6em;">Catégorie</th><th style="width: 6em;">Prix unitaire</th><th style="width: 1em;">Qté</th></tr></thead>\n';
	str+= '</table>\n';
	str+= '<datalist id="listeCategories">\n';
	str+= '  <option value="F0.F001"></option>\n';
	str+= '  <option value="F0.F011"></option>\n';
	str+= '  <option value="F0.F012"></option>\n';
	str+= '  <option value="F0.F013"></option>\n';
	str+= '  <option value="F0.F021"></option>\n';
	str+= '  <option value="F0.F045"></option>\n';
	str+= '</datalist>\n';
	str+= '</div>\n';
	conteneur.innerHTML = str;
	document.querySelector('body').prepend(conteneur);
	var tbodyCourant;
	for(const ligne of s.devis) {
	    if (ligne.first) {
		tbodyCourant = document.createElement('tbody');
		document.querySelector("#afficheDevisTable").appendChild(tbodyCourant);
	    }
	    if (ligne.prixUnitaire != 0.) {
		var nouveauTr = document.createElement('tr');
		str = ''
		str+= '<td><input type="text" size="80" value="" ></td>\n';
		str+= '<td><input type="text" size="7" value="'+ligne.categorie+'" list="listeCategories"></td>\n';
		str+= '<td>'+floatVersStr(ligne.prixUnitaire)+' €</td>\n';
		str+= '<td>'+ligne.quantite+'</td>\n';
		nouveauTr.innerHTML = str;
		nouveauTr.firstChild.firstChild.value = ligne.description;
		tbodyCourant.appendChild(nouveauTr);
	    }
	}
	for(const cat of document.querySelectorAll("#afficheDevisTable tr td:nth-child(2) input")) {
	    cat.addEventListener('blur', function () {propageCategorie(cat)});
	}
	for(const input of document.querySelectorAll("#afficheDevis input")) {
	    input.addEventListener('blur', function () {
		if (inputEstValide(input))
		    input.className = input.className.replace(/\binvalide\b/g, "");});
	}
	document.getElementById('changePrefs').addEventListener('click', function () {
	    var prefs = GM_getValue('prefs');
	    var r = prompt("Code du centre de recherche ? (ex. 04 pour Sophia)", prefs.centre);
	    if (r != null) {
		prefs.centre = r;
		GM_setValue('prefs', prefs);
	    }
	    this.blur();
	});
	var reglesCSS = ''
	reglesCSS+= '#conteneurDevis {\n';
	reglesCSS+= '  position: absolute;\n';
    	reglesCSS+= '  top: 20px;\n';
    	reglesCSS+= '  left: 5%;\n';
    	reglesCSS+= '  right: 5%;\n';
	reglesCSS+= '  z-index: 1000;\n';
	reglesCSS+= '}\n';
	reglesCSS+= '#afficheDevis .invalide {\n';
	reglesCSS+= '  background-color: #ffbbbb\n';
	reglesCSS+= '}\n';
	reglesCSS+= '#afficheDevis {\n';
    	reglesCSS+= '  background-color: whitesmoke;\n';
    	reglesCSS+= '  border: 1px solid gray;\n';
    	reglesCSS+= '  padding: 20px;\n';
    	reglesCSS+= '  margin-bottom: 20px;\n';
    	reglesCSS+= '  border-radius: 8px;\n';
	reglesCSS+= '  overflow: auto;\n';
	reglesCSS+= '}\n';
  	reglesCSS+= '#afficheDevis table {\n';
  	reglesCSS+= '  margin-top: 10px;\n';
  	reglesCSS+= '  border-collapse: collapse;\n';
  	reglesCSS+= '  width: 100%;\n';
	reglesCSS+= '}\n';
	reglesCSS+= '#afficheDevis td, #afficheDevis th {\n';
  	reglesCSS+= '  border: 1px solid gray;\n';
  	reglesCSS+= '  padding: 8px;\n';
	reglesCSS+= '}\n';
	reglesCSS+= '#afficheDevis tr{background-color: #f4e588;}\n';
	reglesCSS+= '#afficheDevis tr:first-child {background-color: #a5bdca;}\n';
	reglesCSS+= '#afficheDevis tr:not(:first-child) td:first-child {padding-left: 30px;}\n';
	reglesCSS+= '#afficheDevis th {\n';
  	reglesCSS+= '  padding-top: 2px;\n';
  	reglesCSS+= '  padding-bottom: 4px;\n';
  	reglesCSS+= '  text-align: center;\n';
  	reglesCSS+= '  background-color: #e0e0d0;\n';
  	reglesCSS+= '  color: black;\n';
	reglesCSS+= '}\n';
	reglesCSS+= '#changePrefs {\n';
	reglesCSS+= '  display: none;\n';
	reglesCSS+= '  font-size: large;\n';
	reglesCSS+= '  padding: 1px 2px 0 2px;\n';
	reglesCSS+= '}\n';
        reglesCSS+= '#cacheModal {\n';
        reglesCSS+= '  width: 100%;\n';
        reglesCSS+= '  height: 100%;\n';
        reglesCSS+= '  background-color: #00000070;\n';
        reglesCSS+= '  z-index: 1001;\n';
        reglesCSS+= '  position: fixed;\n';
        reglesCSS+= '  top: 0;\n';
        reglesCSS+= '  left: 0;\n';
        reglesCSS+= '}\n';
        reglesCSS+= '\n';
        reglesCSS+= '#confirmBox {\n';
        reglesCSS+= '  background-color:#f7f7f7;\n';
        reglesCSS+= '  margin-left: auto;\n';
        reglesCSS+= '  margin-right: auto;\n';
        reglesCSS+= '  position: relative;\n';
        reglesCSS+= '  top: 40px;\n';
        reglesCSS+= '  left: auto;\n';
        reglesCSS+= '  max-width: 780px;\n';
        reglesCSS+= '  border: 1px solid #dcdcdc;\n';
        reglesCSS+= '  font-family: sans-serif;\n';
        reglesCSS+= '  font-size: 14px;\n';
        reglesCSS+= '  line-height: 18px;\n';
        reglesCSS+= '}\n';
        reglesCSS+= '\n';
        reglesCSS+= '#msg {\n';
        reglesCSS+= '  padding: 35px 20px 30px 20px;\n';
        reglesCSS+= '}\n';
        reglesCSS+= '\n';
        reglesCSS+= '#panneauBoutons {\n';
        reglesCSS+= '  background-color: #eaeaea;\n';
        reglesCSS+= '  border: 1px solid #dcdcdc;\n';
        reglesCSS+= '  padding: 14px 25px 18px;\n';
        reglesCSS+= '  text-align: right;\n';
        reglesCSS+= '}\n';
        reglesCSS+= '\n';
        reglesCSS+= '#panneauBoutons #btn1 {\n';
        reglesCSS+= '  margin-right: 10px;\n';
        reglesCSS+= '}\n';
        document.head.insertAdjacentHTML("beforeend", "<style>"+reglesCSS+"</style>");
	$('#afficheDevis tbody').sortable({cancel: "tr:first-child,input",
					   helper: function(e, tr) {
					       var $originals = tr.children();
					       var $helper = tr.clone();
					       $helper.children().each(function(index) {
						   // Set helper cell sizes to match the original sizes
						   $(this).width($originals.eq(index).width());
					       });
					       return $helper;
					   }});
	$('#afficheDevis table').sortable({cancel: "thead,input",
					   helper: function(e, tbody) {
					       var $originals = tbody.children();
					       var $helper = tbody.clone();
					       $helper.children().each(function(indextr) {
						   $(this).children().each(function(index) {
						       // Set helper cell sizes to match the original sizes
						       $(this).width($originals.eq(indextr).children().eq(index).width());
						   })});
					       return $helper;
					   }});
    }

    function logStructureDevis(s) {
	var texte = ''
	texte += 'Devis ' + s.fournisseur + ' n°' + s.numero + ' - clavier ' + s.clavier + '\n';
	texte += 'Position courante : ligne ' + s.i + '\n';
	for(var i=0;i<s.devis.length;i++) {
	    var ligne = s.devis[i];
	    texte += i + ' - ';
	    if (ligne.first) { texte += '* - ';}
	    else { texte += '  - ';}
	    texte += ligne.reference + ' - ';
	    texte += ligne.description + ' - ';
	    texte += ligne.quantite + ' - ';
	    texte += ligne.prixUnitaire + '€\n';
	}
	console.log(texte);
    }

    function standardiseTexte(s) {
	var tmp;
	var r = s;
	tmp = /\s*(.*[^\s])\s*$/s.exec(r);
	if (tmp) { r = tmp[1]; } /* Suppression des blancs initiaux et finaux */
	r = r.replaceAll('\n',' - ');
	r = r.replaceAll('\u009c', 'œ'); /* Les œ de Dell sont bizarrement codés */
	r = r.replaceAll('œ', 'oe'); /* Les œ ne plaisent pas à SafIn */
	return r;
    }

    function prixVersFloat(s) {
        if (s=='') { return 0.; }
        else { return parseFloat(s.replace(',','.').replace(/\s/g,'')); }
    }

    function numeroDevisDell() {
        var divs = document.querySelectorAll('.modal-title');
        var res;
        for (const elem of divs) {
            if (res = /Détails du devis ([0-9]*)/.exec(elem.innerText)) {
                return res[1];
            }
        }
        return "";
    }

    function typeClavierDell(devis) {
        var res;
        for(const elem of devis) {
            if ( (/clavier/i.test(elem.description)) ) {
		if ( res = /(..erty)/i.exec(elem.description) ) {
                    return res[1].toUpperCase();
		}
		else if (/français/i.test(elem.description)) {
		    return "AZERTY";
		}
	    }
        }
        return "";
    }

    function typeClavierEconocom(devis) {
	for(const elem of devis) {
	    if ( (/passage/i.test(elem.description)) &&
		 ( (/américain/i.test(elem.description)) || (/anglais/i.test(elem.description)) )){
		return 'QWERTY';
	    }
	}
	return 'AZERTY';
    }

    function devisEstComplet() {
	var groupes = document.querySelectorAll('#afficheDevisTable tbody');
	if (!groupes) {
	    return false;
	}
	else {
	    var table = document.querySelector('#afficheDevisTable');
	    var inputs = table.querySelectorAll(":not(.hidden) input");
	    inputs = Array.prototype.slice.call(inputs); /* NodeList->Array */
	    inputs.unshift(document.getElementById("equipe"),
			   document.getElementById("numeroDevis"));
	    for (const input of inputs) {
		if (!inputEstValide(input)) {
		    /* Suppression éventuelle de la classe invalide */
		    input.className = input.className.replace(/\binvalide\b/g, "");
		    /* Puis ajout (pour ne pas l'avoir en double) */
		    input.className += "invalide";
		    /* Focus sur l'input fautif */
		    input.focus();
		    return false;
		}
	    }
	    return true;
	}
    }

    function copieDevisStandard() {
	var groupes = document.querySelectorAll('#afficheDevisTable tbody');
	if (!groupes || (groupes.length == 0 )) {
	    alert("Veuillez afficher le devis et recommencer.");
	}
	else {
            var devis = [];
	    for(const groupe of groupes) {
		var lignes = groupe.querySelectorAll('tr');
		var first = true;
		for(const ligne of lignes) {
		    var tds = ligne.querySelectorAll('td');
		    var descr = tds[0].querySelector('input').value;
		    var categorie = tds[1].querySelector('input').value;
		    var prixUnitaire = prixVersFloat(tds[2].innerText);
		    var qtte = parseInt(tds[3].innerText);
		    var ref = '';
		    var ecocontrib = /^Eco contrib./.test(descr);
		    var prixHT = prixUnitaire * qtte;
		    devis.push({"reference": ref, "description": standardiseTexte(descr), "quantite": qtte, "prixUnitaire":prixUnitaire,"prixHT": prixHT, "categorie":categorie, "first":first, "eco":ecocontrib});
		    first = false;
		}
	    }
	    var numeroDevis = document.getElementById("numeroDevis").value;
	    var equipe = document.getElementById("equipe").value;
            var typeClavier = document.getElementById("clavier").value;
	    var fournisseur = document.getElementById("fournisseur").value;
	    var SupplierName = document.getElementById("SupplierName").value;
	    var site = document.getElementById("site").value;
	    var SupplierSiteId = document.getElementById("SupplierSiteId").value;
	    var s = {"i":0, "devis":devis, "fournisseur":fournisseur, "site":site, "SupplierName":SupplierName, "SupplierSiteId":SupplierSiteId, "numero":numeroDevis, "clavier":typeClavier, "equipe":equipe};
            GM_setValue('v', JSON.stringify(s));
	    logStructureDevis(s);
	}
    }

    function copieDevisEconocom() {
	/* Chaque ensemble logique de produits est encapsulé dans un .item-basket */
	var groupes = document.querySelectorAll('.item-basket');
	if (!groupes || (groupes.length == 0)) {
	    alert("Veuillez afficher le détail du panier et recommencer.");
	}
	else {
            var devis = [];
            var i=0; var valmax = 0.; var indexmax=-1; /* Pour la recherche de l'élément principal */

	    for(const groupe of groupes) {
		/* Dans chaque groupe correspondant à un ensemble logique,
		   on trouve un .ligneproduit qui est l'objet principal du groupe,
		   et éventuellement des .option et des .ecotaxe */
		var lignes = groupe.querySelectorAll(':not(.hidden).ligneproduit,:not(.hidden).ecotaxe,:not(.hidden).option');
		var descr = '';
		var qtte = 1;
		for(const ligne of lignes) {
		    var ref = '';
		    var prixUnitaire = 0.;
		    var first = false;
		    var ecocontrib = false;
		    var prixHT = prixVersFloat(ligne.querySelector('.pricetotal,.price_ecotaxe').innerText);
		    if (!ligne.classList.contains('ecotaxe')) {
			/* Une option semble être décrite dans un .produit_nom
			   alors que l'élément principal est décrit dans un .description */
			if (ligne.classList.contains('ligneproduit')) {
			    var elem = ligne.querySelector('.description');
			    descr = elem.querySelector('strong').innerText;
			    ref = elem.querySelector('.ref').innerText.replace('Réf. ','');;
			    first = true;
			}
			else {
			    descr = ligne.querySelector('.produit_nom').innerText;
			    ref = '';
			}
			qtte = parseInt(ligne.querySelector('.qte').value);
		    }
		    else {
			/* Dans le cas d'une ecotaxe, pas de quantité : elle a été lue à la ligne d'avant */
			ecocontrib = true;
			descr = 'Eco contrib. ' + descr;
			ref = 'ECO-'+(''+prixUnitaire).replace('.',',');
		    }
		    /* Le prix unitaire HT est systématiquement déduit du prix
		     * total (cf. issue #1)
		     * https://gitlab.inria.fr/chevilla/AutoDA_equipementInfo/-/issues/1 */
		    /* Pour éviter un double arrondi, on convertit d'abord prixHT en centimes */
		    prixUnitaire = Math.round(prixHT*100)/(qtte*100);
		    if (first && (prixUnitaire > valmax)) {
			valmax = prixUnitaire;
			indexmax = i;
		    }
		    i++;
		    devis.push({"reference": ref, "description": standardiseTexte(descr), "quantite": qtte, "prixUnitaire":prixUnitaire,"prixHT": prixHT, "categorie":"", "first":first, "eco":ecocontrib})
		}
	    }
	    devis = devis.slice(indexmax).concat(devis.slice(0,indexmax));

	    var numeroDevis = "";
	    var equipe = "";
            var typeClavier = typeClavierEconocom(devis);
	    var s = {"i":0, "devis":devis, "fournisseur":"ECONOCOM PRODUCTS AND SOLUTIONS", "site":"CLICHY", "SupplierName": "", "SupplierSiteId":"", "numero":numeroDevis, "clavier":typeClavier, "equipe":equipe};
            GM_setValue('v', JSON.stringify(s));
            alert('Le devis a été copié.');
	    logStructureDevis(s);
	}
    }

    function copieDevisDell() {
        var tableau = document.querySelector('.order-details');
        if (!tableau) {
            alert("Veuillez afficher le devis et recommencer.");
        }
        else {
            var lignes = tableau.querySelector("tbody").getElementsByTagName('tr');
            var devis = []
	    var i=0; var valmax = 0.; var indexmax=-1; /* Pour la recherche de l'élément principal */
            for (const ligne of lignes) {
                var elems = ligne.getElementsByTagName("td");
                var ref = elems[1].innerText;
                var descr = elems[2].innerText;
                var qtte = parseInt(elems[3].innerText);
		var ecocontrib = false;
		var prixUnitaire = prixVersFloat(elems[4].innerText);
		var prixEcoParticipation = prixVersFloat(elems[5].innerText);
                var prixHT = prixVersFloat(elems[6].innerText);

                var res = undefined;
                var first = false;
		if (res = /^Configuration n[^ ]*[\s:]*(.*)/.exec(descr)) {
		    descr = res[1];
		}
                first = ligne.classList.contains("first");
		if ((prixUnitaire == 0.) && (prixEcoParticipation > 0.)) {
		    descr = "Eco contrib. " + descr;
		    ecocontrib = true;
		}
		/* Le prix unitaire HT est systématiquement déduit du prix
		 * total (cf. issue #1)
		 * https://gitlab.inria.fr/chevilla/AutoDA_equipementInfo/-/issues/1 */
		/* Pour éviter un double arrondi, on convertit d'abord prixHT en centimes */
		prixUnitaire = Math.round(prixHT*100)/(qtte*100);
		if (first && (prixUnitaire > valmax)) {
		    valmax = prixUnitaire;
		    indexmax = i;
		}

		i++;
                devis.push({"reference": ref, "description": standardiseTexte(descr), "quantite": qtte, "prixUnitaire":prixUnitaire,"prixHT": prixHT, "categorie":"", "first":first, "eco":ecocontrib})
            }
            var numeroDevis = numeroDevisDell();
            var typeClavier = typeClavierDell(devis);
	    var equipe = "";
	    devis = devis.slice(indexmax).concat(devis.slice(0,indexmax));
	    var s = {"i":0, "devis":devis, "fournisseur":"DELL", "site":"BEZONS", "SupplierName":"", "SupplierSiteId":"", "numero":numeroDevis, "clavier":typeClavier,"equipe":equipe};
            GM_setValue('v', JSON.stringify(s));
            alert('Le devis a été copié.');
	    logStructureDevis(s);
        }
    }

    function copieDevisHPe() {
	/* Chaque ensemble logique de produits est un  ul.cartline  */
	var groupes = document.querySelectorAll('.cartline')
	if (!groupes || (groupes.length == 0)) {
	    alert("Veuillez afficher le panier et recommencer.");
	}
	else {
            var devis = [];
	    var descr;
	    var descrGroupe;
	    var qtte;
	    var qtteGroupe;
	    var ref;
	    var ecocontrib;
	    var prixHT;
	    var prixUnitaire;
	    var first = true;

	    for(const groupe of groupes) {
		first = true;
		var lignes = groupe.querySelectorAll("li");

		/* Pour le groupe tout entier */
		descr = lignes[0].querySelector('a').innerText;
		descrGroupe = descr;
		qtte = parseInt(lignes[lignes.length-3].innerText);
		qtteGroupe = qtte;
		ref = '';
		ecocontrib = false;
		prixHT = prixVersFloat(lignes[lignes.length-1].innerText.replace("EUR HT","").replace(',',''))
		/* Le prix unitaire HT est systématiquement déduit du prix
		 * total (cf. issue #1)
		 * https://gitlab.inria.fr/chevilla/AutoDA_equipementInfo/-/issues/1 */
		/* Pour éviter un double arrondi, on convertit d'abord prixHT en centimes */
		prixUnitaire = Math.round(prixHT*100)/(qtte*100);
		/* TODO : vérifier la conduite à tenir pour l'établissement d'un devis :
		   le détail ne fait que ventiler le prix de l'article du groupe sur des sous-items.
		   Il y a donc deux stratégies possibles, et je ne sais pas laquelle doit être implémentée :
		     - soit on n'affiche que l'article du groupe sans les détails ;
		     - soit on n'affiche que les détails (mais pas l'article lui-même, sinon, il y a redite).
		   En commentant les deux lignes suivantes, je n'affiche que les détails.
		   Pour n'afficher que le groupe et pas les détails, il suffit de mettre afficheDetails à false.
		*/
		// devis.push({"reference": ref, "description": standardiseTexte(descr), "quantite": qtte, "prixUnitaire":prixUnitaire,"prixHT": prixHT, "categorie":"", "first":first, "eco":ecocontrib});
		// first = false;

		/* Le détail, s'il y a lieu */
		var afficheDetails = true;
		if (afficheDetails) {
		    var i;
		    for(i=1;i<=lignes.length-4;i++) {
			var ligne = lignes[i];
			var elems = ligne.firstElementChild.querySelectorAll('div')
			descr = elems[1].innerText;
			if (first) {
			    descr = standardiseTexte(descrGroupe) + " - " + standardiseTexte(descr);
			}
			qtte = parseInt(elems[2].innerText)*qtteGroupe;
			ref = /[^:]*\s*:\s*(.*[^\s])\s*/s.exec(elems[0].innerText);
			if (ref) ref = ref[1];
			else ref = elems[0].innerText.replaceAll('\n','').replaceAll('\t','');
			ecocontrib = false;
			prixUnitaire = prixVersFloat(elems[3].innerText.replace('EUR HT', '').replace(',',''));
			/* Pour éviter un double arrondi, on convertit d'abord prixHT en centimes */
			prixHT = (Math.round(prixUnitaire*100)*qtte)/100; /* Seul le prix unitaire est donné dans le détail. Le prix HT total s'en déduit */
			devis.push({"reference": ref, "description": standardiseTexte(descr), "quantite": qtte, "prixUnitaire":prixUnitaire,"prixHT": prixHT, "categorie":"", "first":first, "eco":ecocontrib});
			first = false;
		    }
		}
	    }
	    var tmp = /Devis n°\s*(.*)/.exec(document.querySelector("div.header").querySelector('ul.row').querySelector('li').innerText)
	    var numeroDevis = (tmp)?(tmp[1]):("")
	    var equipe = "";
            var typeClavier = "";
	    var s = {"i":0, "devis":devis, "fournisseur":"HPe", "site":"", "SupplierName": "", "SupplierSiteId":"", "numero":numeroDevis, "clavier":typeClavier, "equipe":equipe};
            GM_setValue('v', JSON.stringify(s));
            alert('Le devis a été copié.');
	    logStructureDevis(s);
	}
    }

    function soumissionSafin() {
	var lien = document.getElementById("AddToCart").getAttribute("onclick");
	var n = lien.indexOf("AddToCart");
	lien = lien.slice(n);
	lien = lien.slice(0, lien.indexOf("'"));
	GM_setValue('soumis', true);

	submitForm('DefaultFormName',1,{'_FORM_SUBMIT_BUTTON':lien});
    }

    /* Position la variable i du devis stocké dans la structure s sur la
     * prochaine ligne avec un prix non nul.
     *
     * Si on est déjà positionné sur une telle ligne, ne fait rien.
     *
     * Retourne true en cas de succès et false si la fin du devis est atteinte
     * sans trouver de nouvelle ligne.
     */
    function sauteLignesVideDansDevis(s) {
        while ((s.i < s.devis.length) && (s.devis[s.i].prixUnitaire == 0.)) {
            s.i++;
        }
	if (s.i >= s.devis.length) {
	    return false;
	}
	else {
	    return true;
	}
    }

    function verifSafin() {
        var test = document.querySelector('h1[type="OraHeader"]');
        return (test) && (test.innerText == "Demande hors catalogue");
    }

    function recupereDevis() {
        if (!verifSafin()) {
            alert("Veuillez vous positionner sur 'Demande hors catalogue' et recommencer.");
	    return undefined;
        }
        else {
            var v = GM_getValue('v');
            if (!v) {
                alert("Veuillez d'abord copier un devis");
                return undefined;
            }
            var s = JSON.parse(v);
	    return s;
	}
    }

    function colleDevis() {
	var valRetour = true;
        var s = recupereDevis();
	if (!s) return false;
	var estPremiereLigne = (s.i == 0);
        var ligne = {"description":"", "quantite":"", "prixUnitaire":"", "categorie":""};
        if (!sauteLignesVideDansDevis(s)) {
	    /* Plus aucune ligne dans le devis */
	    GM_deleteValue('v');
	    valRetour = false;
        }
        else {
            ligne = s.devis[s.i];
            s.i++;
	    GM_setValue('v', JSON.stringify(s));
        }
	if (estPremiereLigne) {
	    var tmp = GM_getValue('prefs').centre + " - ";
	    tmp += s.equipe + " - ";
	    tmp += ligne.description + " - ";
	    tmp += (s.clavier == "")?"":(s.clavier + " - ");
	    tmp += "Devis " + s.numero;
	    document.getElementById("ItemDescription").value = tmp;
	}
	else {
            document.getElementById("ItemDescription").value = ligne.description;
	}
        document.getElementById("Category").value = ligne.categorie;
        document.getElementById("Quantity").value = ligne.quantite;
        document.getElementById("UnitPrice").value = floatVersStr(ligne.prixUnitaire);

        /* Remplir automatiquement le fournisseur et son site est assez retord. On renonce pour l'instant */
	/*
	  document.getElementById("SupplierOnNonCat").value = s.fournisseur;
	  document.getElementById("SupplierName").value = s.SupplierName;
          document.getElementById("SupplierSiteOnNonCat").value = s.site;
	  document.getElementById("SupplierSiteId").value = s.SupplierSiteId;
	*/
	return valRetour;
    }

    function attendsEtColleDevis(n) {
	if (verifSafin()) {
	    if (colleDevis()) { soumissionSafin(); }
	    else {
		GM_deleteValue('soumis');
	    }
	}
	else {
	    setTimeout(function () {attendsEtColleDevis(n-1);},100);
	}
    }

    function confirmPerso(msgPrincipal, msg1, fn1, msg2, fn2) {
	var cacheModal = document.createElement('div');
	cacheModal.id = 'cacheModal';
	var str = '';
	str += '<div id="confirmBox">\n';
	str += '  <div id="msg">' + msgPrincipal + '</div>\n';
	str += '  <div id="panneauBoutons">\n';
	str += '    <button id="btn1">' + msg1 + '</button>\n';
	str += '    <button id="btn2">' + msg2 + '</button>\n';
	str += '  </div>\n';
	str += '</div>';
	cacheModal.innerHTML = str;
	document.querySelector('body').prepend(cacheModal);
	function emballeFn(fn) {return function() {fn(); cacheModal.remove();}}
	document.querySelector('#btn1').focus();
	document.querySelector('#btn1').addEventListener('click', emballeFn(fn1));
	document.querySelector('#btn2').addEventListener('click', emballeFn(fn2));
    }

    function declenche(e) {
	if ( (e.keyCode == 27) && document.getElementById("conteneurDevis") ) { /* Escape */
	    var conteneurDevis = document.getElementById("conteneurDevis");
	    confirmPerso("Voulez-vous conserver les modifications apportées à votre devis ?",
			 "Enregistrer", function(){copieDevisStandard(); conteneurDevis.remove();},
			 "Quitter sans enregistrer", function() {conteneurDevis.remove();});
	}
	if (e.ctrlKey && e.keyCode == 13) { /* Ctrl + Enter */
	    if (/matinfo/.test(window.location.href)) {
		copieDevisDell();
	    }
	    else if (/econocom/.test(window.location.href)) {
		copieDevisEconocom();
	    }
	    else if (/b2b.hpe.com/.test(window.location.href)) {
		copieDevisHPe();
	    }
	    else {
		if (!document.getElementById("conteneurDevis")) {
		    var s = recupereDevis();
		    if (!s) return;
		    if ( (document.getElementById("SupplierOnNonCat").value == "") ||
			 (document.getElementById("SupplierSiteOnNonCat").value == "") ) {
			alert("Veuillez d'abord sélectionner manuellement le fournisseur et son site.");
			return;
		    }
		    afficheDevis(s);
		}
		else {
		    if (!devisEstComplet()) {
			alert("Veuillez remplir correctement tous les champs avant de soumettre.");
		    }
		    else {
			copieDevisStandard();
			document.getElementById("conteneurDevis").remove();
			colleDevis();
			soumissionSafin();
		    }
		}
	    }
	}
    }

    document.addEventListener('keyup', declenche, false);
    if (GM_getValue('soumis')) {
	attendsEtColleDevis(20);
    }

})();
