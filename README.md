
[//]: # (pandoc README.md -o README.pdf --template eisvogel --number-sections --toc -M title="Manuel d'utilisation d'AutoDA_equipementInfo v5.0" -M author="Sylvain Chevillard" -V lang=fr -V colorlinks -V maketitle)

# Nouveautés

  - Version 5.0 :
    - ajout du nouveau prestataire HPe de Matinfo 5.
  - Version 4.0 :
    - correction d'un bug dans l'affichage du caractère " ;
    - la détection du séparateur décimal préféré est désormais automatique (suppression du menu pour le régler).
  - Version 3.0 :
    - dans le devis, le choix d'un clavier est désormais optionnel ;
    - ajout d'un menu de réglage pour sélectionner le séparateur décimal préféré.
  - Version 2.0 :
    - le caractère « œ » (qui pose problème dans les devis) est désormais remplacé systématiquement par « oe » ;
    - un test vérifie désormais que le devis est complet avant d'autoriser à le valider ;
    - gestion intelligente des arrondis sur chaque unité pour arriver à un total correct du devis ;
    - fenêtre d'enregistrement du devis (quand on appuie sur Echap en cours d'édition d'un devis) plus intuitive ;
    - résolution de plusieurs bugs.

# Présentation
Cet outil est un petit script qui vient ajouter des fonctionnalités à un navigateur internet, pour permettre la saisie rapide de demandes d'achat d'équipements informatiques (marché MatInfo) dans l'outil SafIn.

La prochaine section est un mémo en quelques lignes de ce qu'il faut retenir pour utiliser le script. Les sections suivantes détaillent plus précisément chaque étape.

# Instructions en 20 secondes.
La toute première fois : [installer Tampermonkey](https://www.tampermonkey.net/) sur votre navigateur, et [installer AutoDA_equipementInfo](https://gitlab.inria.fr/chevilla/AutoDA_equipementInfo/-/raw/master/AutoDA_equipementInfo.user.js). **Attention** : bien vérifier, en cliquant sur les liens précédents, qu'ils s'ouvrent avec votre navigateur de prédilection. Si ce n'est pas le cas, faire un clic droit pour copier le lien, puis le coller dans la barre d'adresse du navigateur désiré (ou mieux, modifier l'application par défaut utilisée pour ouvrir les liens).

Par la suite, pour réaliser une DA, se rendre sur le site d'un fournisseur avec les identifiants du compte achats de votre équipe :

  - sur [Dell](https://v2.dell-matinfo.fr/), rubrique « Devis et Commandes en cours > Mes devis », bouton « Voir » du devis qui vous intéresse, puis « Aperçu du devis » ;
  - sur [Econocom](https://sourcing.econocom.com/), rubrique « Mes pièces > Mes paniers sauvegardés > Voir toute la liste », bouton « Prévisualisation » du panier qui vous intéresse. **ATTENTION** : il faut **absolument** que l'agent ait pensé à sauvegarder son panier **avant** de cliquer sur le bouton « Demander un devis ».

Faire `Ctrl + Entrée` pour copier le devis.

Se rendre sur SafIn, dans le formulaire de « Demande hors catalogue », comme pour saisir une DA. Remplir à la main le fournisseur et le site, puis cliquer n'importe où sur la page. Faire `Ctrl + Entrée` pour coller le devis et ouvrir le mode d'édition.

Éditer les éléments manquants dans le devis et faire toute modification que vous jugez appropriée. Faire `Ctrl + Entrée` pour valider le devis. Tous les éléments sont alors ajoutés automatiquement au panier de SafIn.

# Installation
Au préalable, il faut installer l'extension Tampermonkey dans votre navigateur. C'est en fait cette extension qui se chargera d'interpréter les commandes du script `AutoDA_equipementInfo` afin qu'elles s'exécutent dans votre navigateur. L'extension est disponible pour la plupart des navigateurs internet. Pour l'installer : se rendre sur le [site de Tampermonkey](https://www.tampermonkey.net/) avec le navigateur qui servira à effectuer les DA et cliquer sur le bouton « Download » de la version stable.

![Télécharger Tampermonkey](doc/DownloadTampermonkey.png)

Votre navigateur devrait alors automatiquement vous proposer d'installer l'extension : accepter cette proposition et donner les éventuelles autorisations qui sont demandées.

![Installer Tampermonkey. Exemple sous Chrome](doc/InstallerTampermonkey.png)

Il faut à présent faire connaître à Tampermonkey le script `AutoDA_equipementInfo`. Pour ce faire, cliquer simplement sur [ce lien](https://gitlab.inria.fr/chevilla/AutoDA_equipementInfo/-/raw/master/AutoDA_equipementInfo.user.js) (s'assurer qu'il s'ouvre bien avec le navigateur sur lequel on a préalablement installé Tampermonkey). Dans la page qui s'affiche, cliquer sur « Install ».

![Installer AutoDA_equipementInfo](doc/InstallerAutoDA.png)

Normalement, l'installation est essentiellement instantanée. Suivant le navigateur utilisé, l'onglet peut se refermer tout seul ou bien rester ouvert sans rien afficher d'autre qu'une page blanche (il ne s'y passe rien : vous pouvez le fermer sans attendre). Vous pouvez vérifier que le script est bien installé et actif en cliquant sur l'icône de Tampermonkey à droite de la barre d'adresse du navigateur, puis en vous rendant dans le « Tableau de bord » (« Dashboard » en anglais) : il devrait y avoir une ligne correspondant au script `AutoDA_equipementInfo` et il devrait être indiqué comme « activé ».

![Vérification que AutoDA_equipementInfo est bien installé](doc/AutoDA_bienInstalle.png)

# Utilisation
Le script fonctionne sur le principe du « copier-coller », à l'aide d'un unique raccourci clavier : `Ctrl + Entrée`. Utilisé sur le site d'un fournisseur, il copie un devis, tandis qu'utilisé sur SafIn, il le colle. Voyons cela plus en détail.

## Copie depuis le fournisseur Dell
En amont du travail de l'assistante, l'agent (ou le responsable technique de l'équipe) établit un devis sur le [site du marché MatInfo de Dell](https://v2.dell-matinfo.fr/). Une fois le devis émis, l'idéal est de l'enregistrer avec un nom que l'assistante pourra identifier facilement.

Une fois le devis prêt, l'assistante doit saisir la DA. Dans un premier temps, elle se connecte sur le [site du marché MatInfo de Dell](https://v2.dell-matinfo.fr/) avec les identifiants du compte de l'équipe. Se rendre dans la liste des devis (« Devis et Commandes en cours > Mes devis »).

![Recherche d'un devis Dell](doc/DellRechercheDevis.png)

Identifier le devis que l'on doit saisir et cliquer sur « Voir ».

![Sélection d'un devis Dell particulier](doc/DellRechercheDevis2.png)

Les informations générales sur le devis apparaîssent. Cliquer sur « Aperçu du devis » : le détail du devis apparaît.

![Aperçu détaillé d'un devis Dell](doc/DellDevisDetaille.png)

Le raccourci clavier `Ctrl + Entrée` devient opérant : l'utiliser pour copier le devis. Le message « Le devis a été copié » apparaît. Appuyer sur `Entrée` ou cliquer sur « OK » pour le faire disparaître.

![Un devis Dell a été copié](doc/Dell_devisCopie.png)

## Copie depuis le fournisseur HPe
La procédure pour HPe est sensiblement la même que pour Dell. L'agent (ou le responsable technique de l'équipe) se rend sur le [site du marché MatInfo de HPe](https://b2b.hpe.com/) et choisit le matériel. Une fois le panier complet, on émet le devis en cliquant sur « Enregistrer en tant que devis ».

![Établir un devis HPe à partir du panier](doc/HPe_etablirDevis.png)

L'idéal est alors de l'enregistrer avec un nom que l'assistante pourra identifier facilement.

![Enregistrer un devis HPe avec un nom clair](doc/HPe_enregistrerDevis.png)

Une fois le devis prêt, l'assistante doit saisir la DA. Dans un premier temps, elle se connecte sur le [site du marché MatInfo de HPe](https://b2b.hpe.com/) avec les identifiants du compte de l'équipe. Se rendre dans la liste des devis (« Commandes et devis > Devis »).

![Recherche d'un devis HPe](doc/HPeRechercheDevis.png)

Identifier le devis que l'on doit saisir et cliquer sur le nom du devis pour l'ouvrir.

![Sélection d'un devis HPe particulier](doc/HPeRechercheDevis2.png)

Le devis apparaît. Le raccourci clavier `Ctrl + Entrée` devient opérant : l'utiliser pour copier le devis. Le message « Le devis a été copié » apparaît. Appuyer sur `Entrée` ou cliquer sur « OK » pour le faire disparaître.

## Copie depuis le fournisseur Econocom
La procédure est un peu différente pour Econocom car le devis n'est pas généré automatiquement par le site web, mais est envoyé à Econocom où un opérateur se charge d'éditer le devis et de l'envoyer par courrier électronique.

Le script `AutoDA_equipementInfo` travaille à partir de pages web et ne peut donc pas utiliser le devis émis par Econocom. En revanche, il permet de copier le contenu d'un panier, mais il faut donc en faire une sauvegarde avant d'adresser celui-ci à Econocom pour l'établissement du devis. En amont du travail de l'assistante, l'agent (ou le responsable technique de l'équipe) se rend sur le [site d'Econocom](https://sourcing.econocom.com/) pour faire ses choix. **IMPORTANT** : une fois le panier complet, au lieu de faire directement « Demander un devis », l'agent doit cliquer sur « Sauvegarder le panier » et lui donne un nom que l'assistante pourra identifier facilement.

> Note aux beta-testeurs : je pense (mais je ne suis pas sûr) qu'un panier enregistré reste conservé dans les paniers enregistrés, même après avoir été utilisé pour demander un devis. Il n'est pas possible de tester cette hypothèse en simulation, puisqu'elle requiert de solliciter Econocom pour établir un devis. Dans le doute, en attendant de trancher cette question, il peut être sage de sauvegarder deux fois le panier : comme ça, même si celui actif au moment de demander le devis est automatiquement supprimé, il restera l'autre sauvegarde pour travailler.

![Sauvegarde d'un panier Econocom](doc/Econocom_sauvegardePanier.png)

L'agent effectue ensuite sa demande de devis normalement en prévisualisant le panier qu'il vient d'enregistrer, puis en cliquant sur le bouton « Utiliser le panier », et enfin sur le bouton « Demander un devis ».

Une fois le devis reçu, l'assistante va saisir la DA en se basant sur la panier sauvegardé **et non sur le devis**. Se connecter sur le [site d'Econocom](https://sourcing.econocom.com/) avec les identifiants du compte de l'équipe. Se rendre dans la rubrique « Mes pièces ».

![Recherche d'un panier Econocom sauvegardé (1/2)](doc/Econocom_mesPieces.png)

Dans la rubrique « Mes paniers sauvegardés », cliquer sur « Voir toute la liste ».

![Recherche d'un panier Econocom sauvegardé (2/2)](doc/Econocom_voirListe.png)

Enfin, cliquer sur le bouton de prévisualisation du panier concerné.

![Visualisation d'un panier Econocom sauvegardé](doc/Econocom_chargerPanier.png)

Le détail du panier apparaît. Le raccourci clavier `Ctrl + Entrée` devient opérant : l'utiliser pour copier le devis. Le message « Le devis a été copié » apparaît. Appuyer sur `Entrée` ou cliquer sur « OK » pour le faire disparaître.

![Un devis Econocom a été copié](doc/Econocom_devisCopie.png)

## Collage dans SafIn
Une fois le devis copié depuis le site du fournisseur, il reste à le coller dans SafIn. Pour cela, l'assistante se rend dans SafIn à la rubrique « Demande hors catalogue » comme elle le ferait pour saisir manuellement la DA. Là, il faut tout d'abord remplir manuellement le champ « Fournisseur » et le champ « Site ».

![En cas d'oubli, le raccourci `Ctrl + Entrée` est inopérant et affiche simplement un rappel](doc/Safin_avertissementFournisseur.png)

Ensuite, cliquer n'importe où dans la page afin de faire perdre le focus aux champs en question, et utiliser `Ctrl + Entrée`.

Le mode d'édition du devis apparaît. Celui-ci reprend toutes les entrées du devis dont le prix n'est pas nul. En outre, le modèle de clavier devrait automatiquement être reconnu. S'il s'agit d'un devis Dell, le numéro de devis est également renseigné. En revanche, le nom de l'équipe est à renseigner explicitement.

![Mode d'édition tel qu'il apparaît juste après avoir fait `Ctrl + Entrée`](doc/Safin_modeEdition.png)

Chaque élément principal du devis apparaît en bleu, avec les options afférentes en dessous en jaune. Il est possible de réorganiser l'ordre du devis : en cliquant et glissant une ligne bleue, on déplace le bloc formé de cette ligne et de toutes ses options ; en cliquant et glissant une ligne jaune, on déplace une option au sein d'un bloc.

![Déplacement d'un bloc en cliquant-glissant une ligne bleue](doc/Safin_deplacementBloc.png)

La description de chaque ligne est éditable au besoin. **Important** : pour la première ligne, renseigner uniquement la description de l'article ; les autres informations qui doivent figurer sur la première ligne d'une DA (équipe, numéro de devis, etc.) seront ajoutées automatiquement lors de l'import vers SafIn.

Le champ « Catégorie » est intelligent : il connaît la liste des nomenclatures possibles et propose uniquement les catégories contenant la chaîne de caractères déjà tapés. Par exemple, si on tape « 2 », seules les catégories « F0.F012 » et « F0.F021 » apparaîssent. On peut alors sélectionner la bonne catégorie, soit en cliquant dessus, soit en utilisant les flèches haut et bas du clavier et en validant avec `Entrée`. Lorsqu'on quitte le champ « Catégorie » d'un article (en cliquant dans un autre champ, ou bien en faisant `Tabulation`), toutes les lignes situées en dessous de celle qui vient d'être remplie, et dans le même bloc, voient automatiquement leur catégorie renseignée avec cette même valeur. De cette manière, si on a pris soin préalablement de regrouper les lignes par type de produit, on peut en procédant de haut en bas, n'éditer la catégorie que du premier article de chaque catégorie.

![Un devis rempli, prêt à être validé.](doc/Safin_devisPret.png)

À tout instant, il est possible de quitter le mode d'édition du devis en appuyant sur la touche `Echap`. Une boîte de dialogue permet alors, au choix, de sauvegarder les modifications déjà apportées ou bien d'effacer toutes les modifications. On revient ultérieurement au mode d'édition en utilisant à nouveau le raccourci `Ctrl + Entrée`.

![Boîte de dialogue qui apparaît lorsqu'on quitte le mode d'édition avec la touche `Echap`](doc/Safin_sortieModeEdition.png)

Enfin, dans le mode d'édition du devis, lorsque tous les champs sont remplis et que celui-ci est satisfaisant, utiliser à nouveau le raccourci `Ctrl + Entrée` pour le valider. Toutes les lignes sont automatiquement (et successivement ; cela prend quelques secondes) ajoutées au panier SafIn.

![Pendant le remplissage automatique du panier SafIn. Ici, la 7e ligne vient d'être ajoutée](doc/Safin_importEnCours.png)

![Panier SafIn entièrement rempli. Le remplissage s'est fait au rythme d'une ligne par seconde environ](doc/Safin_importFini.png)
